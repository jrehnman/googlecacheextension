
.PHONY: clean

PACKAGE_NAME=cacheextension.zip
CONVERTED_ICONS=icon128.png icon48.png
LOCALES=$(wildcard _locales/*/messages.json)

%.png: %.xcf
	xcf2png $^ -o $@

$(PACKAGE_NAME): manifest.json callbackRegister.js icon16.png $(CONVERTED_ICONS) $(LOCALES)
	zip -r $@ $^

clean:
	rm -vf $(PACKAGE_NAME) $(CONVERTED_ICONS)

