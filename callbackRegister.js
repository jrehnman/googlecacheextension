

var OPEN_CACHED_MENUID = "openCached";
var OPEN_CACHED_TAB_MENUID = "openCachedTab";

var googleTrackingPattern = /http(?:s)?:\/\/[^/]*\.google\.[^/.]+\/url\?(?:.*&)?url=([^&]+)/i;

/*
 * Google rewrites links on the search result page to track user clicks.
 * This function parses out the target url in rewritten links.
 */
function decodeGoogleTracking(url)
{
    var matches = googleTrackingPattern.exec(url);
    if (matches && matches.length > 1)
    {
        var encodedTrackedUrl = matches[1];
        var trackedUrl = decodeURIComponent(encodedTrackedUrl);
        return trackedUrl;
    }
    else
    {
        return url;
    }
}

function onMenuClick(info, tab)
{
    var linkUrl = decodeGoogleTracking(info.linkUrl);
    var cacheUrl = "http://www.google.com/search?q=cache:" + linkUrl;

    if (info.menuItemId == OPEN_CACHED_MENUID)
    {
        if (tab)
        {
            chrome.tabs.update(tab.id, {"url": cacheUrl});
        }
    }
    else if (info.menuItemId == OPEN_CACHED_TAB_MENUID)
    {
        chrome.tabs.create( {"url": cacheUrl} );
    }
}

chrome.contextMenus.create( {
    "id": OPEN_CACHED_MENUID,
    "title": chrome.i18n.getMessage("openCachedMenu"),
    "contexts": ["link"],
    } );

chrome.contextMenus.create( {
    "id": OPEN_CACHED_TAB_MENUID,
    "title": chrome.i18n.getMessage("openCachedTabMenu"),
    "contexts": ["link"],
    } );

chrome.contextMenus.onClicked.addListener(onMenuClick);
